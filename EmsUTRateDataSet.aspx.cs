using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Drawing;
using MFGReportData;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;

public partial class FABReport_MOD_EmsUTRateDataSet : System.Web.UI.Page
{
    string m_ShopIndex;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.PreviousPage != null)
        {

            // *** Get Page Setting from System Info ***
            string strPageWidth = "0";
            string strGroupby = "ENTITY_STATUS, EQ_STATUS, C_DESC, CODE";
            string strGroupby2 = "ACCT_DATE";
            string strGroupby3 = "ENTITY_STATUS";
            string strCategory = "";
            string sumUSETIME = "";
            string strSQLCondition = "";
            double sum = 0;
            double EQtotal = 0;
            double RunTime = 0;
            double IdleTime = 0;
            double ProCrashTotal = 0;
            double EQPCrashTotal = 0;
            double ProCrashTotalSum = 0;
            double EQPCrashTotalSum = 0;
            double EQPMaintenance = 0;
            double EQPMaintenanceSum = 0;
            double RunIdleSum = 0;
            double PercentRate = 0;
            int index = 0;
            int indexrow = 0;
            int RowCount = 0;
            int dtcount = 0;
            int[] arrABCLocal = new int[3];
            m_ShopIndex = txnQuery.GetPageInfo("Shop");
            RTData_Pass obj = new RTData_Pass(m_ShopIndex);
            RTData_Common ConnObj = new RTData_Common(m_ShopIndex);
            RTData_Entity EntityObj = new RTData_Entity(m_ShopIndex);

            strPageWidth = txnQuery.GetPageInfo("ReportWidth");
            if (strPageWidth != "0")
            {
                hReportWidth.Value = strPageWidth;
                gv1.Width = strPageWidth;
            }
            ddmDetail.ImportFormInformation(txnQuery.ExportFormInformation());

            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();
            DataTable dt3 = new DataTable();
            DataTable dtTransUSEITEM = new DataTable();
            DataTable dtEQPRate = new DataTable();
            DataTable dtCrashRate = new DataTable();
            RTData_Pass P = new RTData_Pass(m_ShopIndex);

            string strPeriod = txnQuery.GetQueryInfo("qryPeriod", "SQLCondition");
            List<string> lstSQLCondition = new List<string>();

            strSQLCondition = txnQuery.GetQueryInfo("qShift", "SQLCondition");
            if (!String.IsNullOrEmpty(strSQLCondition)) lstSQLCondition.Add(strSQLCondition);
            strSQLCondition = txnQuery.GetQueryInfo("qryTimeType", "Text");
            if (!String.IsNullOrEmpty(strSQLCondition))
            {
                if (strSQLCondition == "Shift")
                {
                    strCategory = "S";
                    strGroupby += ", ACCT_DATE";
                    strGroupby += ", SHIFT_ID";
                    strGroupby2 += ", SHIFT_ID";
                    strGroupby3 += ", ACCT_DATE";
                    strGroupby3 += ", SHIFT_ID";
                }
                else if (strSQLCondition == "Day")
                {
                    strGroupby += ", ACCT_DATE";
                    strGroupby3 += ", ACCT_DATE";
                }
                else if (strSQLCondition == "Week")
                {
                    strCategory = "W";
                    strGroupby += ", MFGWEEK";
                    strGroupby2 = strGroupby2.Replace("ACCT_DATE", "MFGWEEK");
                    strGroupby3 += ", MFGWEEK";
                }
                else if (strSQLCondition == "Month")
                {
                    strCategory = "M";
                    strGroupby += ", MFGMONTH";
                    strGroupby2 = strGroupby2.Replace("ACCT_DATE", "MFGMONTH");
                    strGroupby3 += ", MFGMONTH";
                }
                else if (strSQLCondition == "Equipment")
                {
                    strCategory = "EQ";
                    strGroupby += ", EQUIP_NBR";
                    strGroupby2 = strGroupby2.Replace("ACCT_DATE", "EQUIP_NBR");
                    strGroupby3 += ", EQUIP_NBR";
                }
                else if (strSQLCondition == "Equipment+Shift")
                {
                    strCategory = "EQS";
                    strGroupby += ", EQUIP_NBR";
                    strGroupby += ", SHIFT_ID";
                    strGroupby2 = strGroupby2.Replace("ACCT_DATE", "EQUIP_NBR");
                    strGroupby2 += ", SHIFT_ID";
                    strGroupby3 += ", EQUIP_NBR";
                    strGroupby3 += ", SHIFT_ID";
                }
                else
                {
                    strCategory = null;
                }
            }

            strSQLCondition = txnQuery.GetQueryInfo("qFloor", "SQLCondition");
            if (!String.IsNullOrEmpty(strSQLCondition)) lstSQLCondition.Add(strSQLCondition);
            strSQLCondition = txnQuery.GetQueryInfo("qEquipment", "SQLCondition");
            if (!String.IsNullOrEmpty(strSQLCondition)) lstSQLCondition.Add(strSQLCondition);
            else if (strCategory == "EQ" || strCategory == "EQS")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "", "alert('請選擇機台,否則在X軸無法show全部機台')", true);
                return;
            }

            lstSQLCondition.Add("equip_nbr<>' '");

            dt = EntityObj.GetEQStatusSUM(strPeriod, lstSQLCondition, strGroupby, "CODE");

            dt2 = obj.GetMoveOutSUM(strPeriod, lstSQLCondition, strGroupby2, "CODE|" + strCategory, true);

            dt3 = EntityObj.GetEQStatusSUM(strPeriod, lstSQLCondition, strGroupby3, "CODE");

            //沒有資料會有Error
            if (dt == null || dt.Rows.Count == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "", "alert('No Data For Your Query')", true);
                return;
            }

            //日期名稱統一
            if (strCategory == "W")
            {
                dt.Columns["MFGWEEK"].ColumnName = "ACCT_DATE";
                dt2.Columns["MFGWEEK"].ColumnName = "ACCT_DATE";
                dt3.Columns["MFGWEEK"].ColumnName = "ACCT_DATE";
            }
            else if (strCategory == "M")
            {
                dt.Columns["MFGMONTH"].ColumnName = "ACCT_DATE";
                dt2.Columns["MFGMONTH"].ColumnName = "ACCT_DATE";
                dt3.Columns["MFGMONTH"].ColumnName = "ACCT_DATE";
            }
            else if (strCategory == "EQ" || strCategory == "EQS")
            {
                dt.Columns["EQUIP_NBR"].ColumnName = "ACCT_DATE";
                dt2.Columns["EQUIP_NBR"].ColumnName = "ACCT_DATE";
                dt3.Columns["EQUIP_NBR"].ColumnName = "ACCT_DATE";
            }

            dt2.Columns.Add("C_DESC", typeof(string));
            if (dt.Columns.Contains("ACCT_DATE"))
            {
                dt.Columns["ACCT_DATE"].MaxLength = 50;
                dt2.Columns["ACCT_DATE"].MaxLength = 50;
                dt3.Columns["ACCT_DATE"].MaxLength = 50;
            }
            //ACCT_DATE+班別 or EQP_NBR++班別
            if (strCategory == "S" || strCategory == "EQS")
            {
                dt.Columns.Add("TempACCT_DATE", typeof(string)).SetOrdinal(5);

                foreach (DataRow dr in dt.Rows)
                {
                    if (strCategory != "EQS")
                    {
                        DateTime Date = Convert.ToDateTime(dr["ACCT_DATE"].ToString());
                        string DateString = Date.ToString("yyyy-MM-dd");
                        dr["TempACCT_DATE"] = DateString + dr["SHIFT_ID"];
                    }
                    else
                    {
                        dr["TempACCT_DATE"] = dr["ACCT_DATE"].ToString() + dr["SHIFT_ID"].ToString();
                    }
                }
                foreach (DataRow dr in dt2.Rows)
                {
                    if (strCategory != "EQS")
                    {
                        DateTime Date = Convert.ToDateTime(dr["ACCT_DATE"].ToString());
                        string DateString = Date.ToString("yyyy-MM-dd");
                        dr["ACCT_DATE"] = DateString + dr["SHIFT_ID"];
                    }
                    else
                    {
                        dr["ACCT_DATE"] = dr["ACCT_DATE"].ToString() + dr["SHIFT_ID"].ToString();
                    }

                    dr["C_DESC"] = "生產台數";

                    //USETIMETotal += double.Parse(dr[0].ToString());
                }
                foreach (DataRow dr in dt3.Rows)
                {
                    //ACCT_DATE + SHIFT_ID
                    index = dt3.Rows.IndexOf(dr);
                    if (strCategory != "EQS")
                    {
                        DateTime Date = Convert.ToDateTime(dr["ACCT_DATE"].ToString());
                        string DateString = Date.ToString("yyyy-MM-dd");
                        dr["ACCT_DATE"] = DateString + dr["SHIFT_ID"];
                    }
                    else
                    {
                        dr["ACCT_DATE"] = dr["ACCT_DATE"].ToString() + dr["SHIFT_ID"].ToString();
                    }

                    //Rename if ENTITY_STATUS is NULL
                    if (dr[0].ToString() == "")
                    {
                        dt3.Rows[index][0] = "OTHER";
                    }
                }
                dt.Columns.Remove("SHIFT_ID");
                dt2.Columns.Remove("SHIFT_ID");
                dt3.Columns.Remove("SHIFT_ID");
                dt.Columns.Remove("ACCT_DATE");
                dt.Columns["TempACCT_DATE"].ColumnName = "ACCT_DATE";
            }
            else
            {
                foreach (DataRow dr in dt2.Rows)
                {
                    dr["C_DESC"] = "生產台數";
                }
                foreach (DataRow dr in dt3.Rows)
                {
                    //Rename if ENTITY_STATUS is NULL
                    if (dr[0] == DBNull.Value)
                    {
                        dt3.Rows[index][0] = "OTHER";
                    }
                }
            }
            dt2.Columns.Remove("RWK_PNL_CNT");
            dt2 = ConnObj.ExtendByColumn(dt2, "ACCT_DATE", "PNL_CNT", true, true);
            dt2.Columns.Add("CODE", typeof(string)).SetOrdinal(0);

            dt3 = ConnObj.ExtendByColumn(dt3, "ACCT_DATE", "USETIME", true, true);


            dtTransUSEITEM = dt.Copy();
            dtTransUSEITEM.Columns.Remove("ENTITY_STATUS");
            dtTransUSEITEM.Columns.Remove("EQ_STATUS");
            if (strCategory == "DS")
            {
                dtTransUSEITEM.Columns.Remove("SHIFT_ID");
            }
            dtTransUSEITEM = ConnObj.ExtendByColumn(dtTransUSEITEM, "ACCT_DATE", "USETIME", true, true);
            dtTransUSEITEM.Columns["C_DESC"].SetOrdinal(1);

            dt = dtTransUSEITEM.Copy();

            dt.Columns.Add("TYPE", typeof(string));
            dt.Columns.Add("TAL", typeof(double));
            dt.Columns.Add("PERCENT", typeof(double));

            dt2.Columns.Add("TAL", typeof(double));
            dt2.Columns.Add("PERCENT", typeof(double));

            //TPYE ABC
            foreach (DataRow dr in dt.Rows)
            {
                index = dt.Rows.IndexOf(dr);


                if (dr["code"].ToString().StartsWith("100"))
                {
                    dr["TYPE"] = "run";
                }
                else if (dr["code"].ToString().StartsWith("200"))
                {
                    dr["TYPE"] = "Idle";
                }
                else if (dr["code"].ToString().StartsWith("41"))
                {
                    dr["TYPE"] = "A";
                }
                else if (dr["code"].ToString().StartsWith("48") && !dr["code"].ToString().StartsWith("4820") && !dr["code"].ToString().StartsWith("4821") && !dr["code"].ToString().StartsWith("4822"))
                {
                    dr["TYPE"] = "B";
                }
                else
                {
                    dr["TYPE"] = "C";
                }
            }

            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                if (!dt.Rows[i]["CODE"].ToString().StartsWith("4"))
                {
                    dt.Rows.RemoveAt(i);
                }
            }

            //Sort
            DataView dv = dt.DefaultView;
            dv.Sort = "TYPE, CODE";
            dt = dv.ToTable();

            //百分比分母計算
            PercentRate = 0;
            foreach (DataRow dr in dt3.Rows)
            {
                sum = 0;
                foreach (DataColumn dc in dt3.Columns)
                {
                    index = dt3.Columns.IndexOf(dc);
                    if (index >= 1 && !string.IsNullOrEmpty(dr[dc].ToString()))
                    {
                        PercentRate += double.Parse(dr[dc].ToString());
                    }
                }
            }

            //計算Total
            foreach (DataRow dr in dt.Rows)
            {
                sum = 0;
                foreach (DataColumn dc in dt.Columns)
                {
                    index = dt.Columns.IndexOf(dc);
                    if (index >= 2 && !string.IsNullOrEmpty(dr[dc].ToString()) && dt.Columns[index].ColumnName != "TYPE")
                    {
                        sum += double.Parse(dr[dc].ToString());
                    }
                    if (dt.Columns[index].ColumnName == "TYPE")
                    {
                        break;
                    }
                }
                dr["TAL"] = sum;
                dr["PERCENT"] = sum / PercentRate * 100;
            }

            DataTable dt_TempName = dt.Clone();

            //總計
            sum = 0;
            foreach (DataColumn dc in dt2.Columns)
            {
                index = dt2.Columns.IndexOf(dc);
                if (index >= 2 && !string.IsNullOrEmpty(dt2.Rows[0][index].ToString()) && dt2.Columns[index].ColumnName != "TAL")
                {
                    sum += double.Parse(dt2.Rows[0][index].ToString());
                }
                if (dt2.Columns[index].ColumnName == "TAL")
                {
                    break;
                }
                dt2.Rows[0]["TAL"] = sum;
                dt2.Rows[0]["PERCENT"] = sum / PercentRate * 100;
            }

            //列加總 + 轉置準備
            foreach (DataColumn dc in dt.Columns)
            {
                index = dt.Columns.IndexOf(dc);
                if (index >= 2)
                {
                    if (dt.Columns[index].ColumnName != "TYPE" && dt.Columns[index].ColumnName != "TAL")
                    {
                        dt.Columns[index].ColumnName = "USETIME" + index;
                        if (!string.IsNullOrEmpty(dt.Rows[RowCount][index].ToString()))
                        {
                            sum += double.Parse(dt.Rows[RowCount][index].ToString());
                        }
                    }
                    else if (dt.Columns[index].ColumnName == "TAL")
                    {
                        dt.Columns[index].ColumnName = "TAL";
                        sum += double.Parse(dt.Rows[RowCount][index].ToString());
                    }
                    if (string.IsNullOrEmpty(sumUSETIME) && dt.Columns[index].ColumnName != "TYPE" && dt.Columns[index].ColumnName != "TAL")
                    {
                        sumUSETIME += "SUM(USETIME" + index + ")";
                    }
                    else if (dt.Columns[index].ColumnName != "TYPE" && dt.Columns[index].ColumnName != "TAL")
                    {
                        sumUSETIME += ", SUM(USETIME" + index + ")";
                    }
                    else if (dt.Columns[index].ColumnName == "TAL")
                    {
                        sumUSETIME += ", SUM(TAL)";
                    }
                    else if (dt.Columns[index].ColumnName == "PERCENT")
                    {
                        sumUSETIME += ", SUM(PERCENT)";
                    }
                }
            }

            DataTable dt_calculate_time = ConnObj.GroupBy(dt, "TYPE", sumUSETIME);
            dt_calculate_time.Columns.Add("C_DESC").SetOrdinal(1);

            //命名ABC
            foreach (DataRow dr in dt_calculate_time.Rows)
            {
                if (dr["TYPE"].ToString() == "A")
                {
                    dr["C_DESC"] = "PM Sum";
                }
                else if (dr["TYPE"].ToString() == "B")
                {
                    dr["C_DESC"] = "Down Sum";
                }
                else if (dr["TYPE"].ToString() == "C")
                {
                    dr["C_DESC"] = "Other Sum";
                }
            }

            //轉至完將命名變更回來
            foreach (DataColumn dc in dt.Columns)
            {
                index = dt.Columns.IndexOf(dc);
                if (index >= 2 && index <= dt_calculate_time.Columns.Count - 3 && dt_TempName.Columns[index].ColumnName != "TYPE")
                {
                    dt_calculate_time.Columns[index].ColumnName = dt_TempName.Columns[index].ColumnName;
                }
                else if (index >= 2 && index <= dt_calculate_time.Columns.Count - 1)
                {
                    dt_calculate_time.Columns[index].ColumnName = dt_TempName.Columns[index + 1].ColumnName;
                }

                if (index >= 2 && index <= dt.Columns.Count - 1)
                {
                    dt.Columns[index].ColumnName = dt_TempName.Columns[index].ColumnName;
                }
            }

            dt_calculate_time.Columns["TYPE"].ColumnName = "CODE";

            dtcount = dt_calculate_time.Rows.Count;

            DataTable TempTable = dt.Clone();

            //插入ABC
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                if (dtcount != 0)
                {
                    if (dt.Rows[i]["TYPE"].ToString() == dt_calculate_time.Rows[dt_calculate_time.Rows.Count - dtcount]["CODE"].ToString())
                    {
                        //插入ABC+Row
                        TempTable.ImportRow(dt_calculate_time.Rows[dt_calculate_time.Rows.Count - dtcount]);
                        TempTable.ImportRow(dt.Rows[i]);
                        dtcount -= 1;
                        //顏色註記
                        arrABCLocal[2 - dtcount] = i + 2 - dtcount;
                    }
                    else
                    {
                        //插入Row
                        TempTable.ImportRow(dt.Rows[i]);

                    }
                }
                else
                {
                    //插入Row
                    TempTable.ImportRow(dt.Rows[i]);

                }
            }
            TempTable.Columns.Remove("TYPE");
            dt = TempTable.Copy();

            dtEQPRate = dt.Clone();
            dtEQPRate.Columns["PERCENT"].DataType = typeof(string);
            dtEQPRate.Rows.Add("", "Up Time Rate");
            dtEQPRate.Rows.Add("", "製程當機率");
            dtEQPRate.Rows.Add("", "設備保養率");
            dtEQPRate.Rows.Add("", "設備當機率");
            dtEQPRate.Rows.Add("", "設備LOSS率");
            foreach (DataColumn dc in dt.Columns)
            {
                index = dt.Columns.IndexOf(dc);
                if (index >= 1)
                {
                    EQtotal = 0;
                    //UpTimeRate
                    foreach (DataRow dr in dt3.Rows)
                    {
                        indexrow = dt3.Rows.IndexOf(dr);
                        if (index <= dt3.Columns.Count - 1)
                        {
                            if (dt3.Rows[indexrow][index] != DBNull.Value)
                            {
                                EQtotal += double.Parse(dt3.Rows[indexrow][index].ToString());
                            }
                            if (dt3.Rows[indexrow]["ENTITY_STATUS"].ToString() == "RUN" && dt3.Rows[indexrow][index] != DBNull.Value)
                            {
                                RunTime = double.Parse(dt3.Rows[indexrow][index].ToString());
                            }
                            if (dt3.Rows[indexrow]["ENTITY_STATUS"].ToString() == "IDLE" && dt3.Rows[indexrow][index] != DBNull.Value)
                            {
                                IdleTime = double.Parse(dt3.Rows[indexrow][index].ToString());
                            }
                        }
                    }
                    if (index <= dt3.Columns.Count - 1)
                    {
                        dtEQPRate.Rows[0][index + 1] = Math.Round((RunTime + IdleTime) / EQtotal * 100, 3, MidpointRounding.AwayFromZero);
                        RunIdleSum += RunTime + IdleTime;
                    }
                    else if (index <= dt3.Columns.Count)
                    {
                        dtEQPRate.Rows[0][index + 1] = Math.Round(RunIdleSum / PercentRate * 100, 3, MidpointRounding.AwayFromZero);
                    }
                    else if (index <= dt3.Columns.Count + 1)
                    {
                        dtEQPRate.Rows[0][index + 1] = "Up Time Rate";
                    }

                    EQPCrashTotal = 0;
                    EQPMaintenance = 0;
                    ProCrashTotal = 0;
                    if (index >= 1)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            indexrow = dt.Rows.IndexOf(dr);
                            if (index >= 1 && index <= dt.Columns.Count - 4)
                            {
                                //製程當機率
                                indexrow = dt.Rows.IndexOf(dr);
                                if ((dt.Rows[indexrow][0].ToString() == "4820" || dt.Rows[indexrow][0].ToString() == "4821" || dt.Rows[indexrow][0].ToString() == "4822") && dt.Rows[indexrow][index + 1] != DBNull.Value)
                                {
                                    ProCrashTotalSum += double.Parse(dt.Rows[indexrow][index + 1].ToString());
                                    ProCrashTotal += double.Parse(dt.Rows[indexrow][index + 1].ToString()) / EQtotal * 100;
                                }
                                //設備保養率
                                else if (dt.Rows[indexrow][0].ToString() == "A" && dt.Rows[indexrow][index + 1] != DBNull.Value)
                                {
                                    EQPMaintenanceSum += double.Parse(dt.Rows[indexrow][index + 1].ToString());
                                    EQPMaintenance += double.Parse(dt.Rows[indexrow][index + 1].ToString()) / EQtotal * 100;
                                }
                                //設備當機率
                                else if (dt.Rows[indexrow][0].ToString() == "B" && dt.Rows[indexrow][index + 1] != DBNull.Value)
                                {
                                    EQPCrashTotalSum += double.Parse(dt.Rows[indexrow][index + 1].ToString());
                                    EQPCrashTotal += double.Parse(dt.Rows[indexrow][index + 1].ToString()) / EQtotal * 100;
                                }
                            }

                            //計算完的欄位取小數點第二位, 百分比取第三位
                            if (index < dt.Columns.Count - 1)
                            {
                                if (dt.Rows[indexrow][index + 1] != DBNull.Value)
                                {
                                    if (dt.Columns[index + 1].ColumnName == "PERCENT")
                                    {
                                        dt.Rows[indexrow][index + 1] = Math.Round(double.Parse(dt.Rows[indexrow][index + 1].ToString()), 3, MidpointRounding.AwayFromZero);
                                    }
                                    else if (index >= 1)
                                    {
                                        dt.Rows[indexrow][index + 1] = Math.Round(double.Parse(dt.Rows[indexrow][index + 1].ToString()), 2, MidpointRounding.AwayFromZero);
                                    }
                                }
                            }
                        }
                        if (index <= dt.Columns.Count - 4)
                        {
                            dtEQPRate.Rows[1][index + 1] = Math.Round(ProCrashTotal, 3, MidpointRounding.AwayFromZero);
                            dtEQPRate.Rows[2][index + 1] = Math.Round(EQPMaintenance, 3, MidpointRounding.AwayFromZero);
                            dtEQPRate.Rows[3][index + 1] = Math.Round(EQPCrashTotal, 3, MidpointRounding.AwayFromZero);
                            //設備LOSS率
                            dtEQPRate.Rows[4][index + 1] = Math.Round(ProCrashTotal + EQPMaintenance + EQPCrashTotal, 3, MidpointRounding.AwayFromZero);
                        }
                        else if (index <= dt.Columns.Count - 3)
                        {
                            dtEQPRate.Rows[1][index + 1] = Math.Round(ProCrashTotalSum / PercentRate * 100, 3, MidpointRounding.AwayFromZero);
                            dtEQPRate.Rows[2][index + 1] = Math.Round(EQPMaintenanceSum / PercentRate * 100, 3, MidpointRounding.AwayFromZero);
                            dtEQPRate.Rows[3][index + 1] = Math.Round(EQPCrashTotalSum / PercentRate * 100, 3, MidpointRounding.AwayFromZero);
                            //設備LOSS率
                            dtEQPRate.Rows[4][index + 1] = Math.Round((ProCrashTotalSum + EQPMaintenanceSum + EQPCrashTotalSum) * 100 / PercentRate, 3, MidpointRounding.AwayFromZero);
                        }
                        else if (index <= dt.Columns.Count - 2)
                        {
                            dtEQPRate.Rows[1][index + 1] = "製程當機率";
                            dtEQPRate.Rows[2][index + 1] = "設備保養率";
                            dtEQPRate.Rows[3][index + 1] = "設備當機率";
                            //設備LOSS率
                            dtEQPRate.Rows[4][index + 1] = "設備LOSS率";
                        }
                    }
                }
            }

            dt.Columns.Add("PERCENTTemp", typeof(string));
            dt2.Columns.Add("PERCENTTemp", typeof(string));
            //轉字串+百分比符號
            foreach (DataRow dr in dt.Rows)
            {
                dr["PERCENTTemp"] = dr["PERCENT"].ToString() + " %";
            }
            dt.Columns.Remove("PERCENT");
            dt.Columns["PERCENTTemp"].ColumnName = "PERCENT";

            dt2.Rows[0]["PERCENTTemp"] = "生產台數";
            dt2.Columns.Remove("PERCENT");
            dt2.Columns["PERCENTTemp"].ColumnName = "PERCENT";
            dt2.Columns["PERCENT"].ColumnName = "PERCENT";

            dt.Columns["CODE"].AllowDBNull = true;
            dt.Merge(dt2);
            dt.Merge(dtEQPRate);

            while (dt.Columns[dt.Columns.Count - 1].ToString() != "PERCENT")
            {
                dt.Columns.RemoveAt(dt.Columns.Count - 1);
            }

            DataTable dtMapping = new RTData_Common().CreateMappingTable(dt);
            if (dtMapping != null && dt.Rows.Count > 0)
            {
                new RTData_Dictionary(m_ShopIndex).TranslateMappingTable(dtMapping);
                gv1.SetColumnMappingTable(dtMapping);
            }

            #region 處理 gvGrid

            gv1.Visible = true;

            gv1.Title = m_ShopIndex + " Ems Pareto Data";
            gv1.ToolBarTitle = m_ShopIndex + " Ems Pareto Data";
            gv1.EnableScroll = true;

            gv1.SetData(dt);

            //ABC Color
            foreach (int i in arrABCLocal)
            {
                gv1.GetRow(i).BackColor = System.Drawing.ColorTranslator.FromHtml("#80ff80");
            }
            //生產台數 Color
            gv1.GetRow(gv1.RowCount - 6).BackColor = System.Drawing.ColorTranslator.FromHtml("#ffd0ff");
            //Up Time Rate Color
            gv1.GetRow(gv1.RowCount - 5).BackColor = System.Drawing.ColorTranslator.FromHtml("#34fa97");
            //製程當機率 Color
            gv1.GetRow(gv1.RowCount - 4).BackColor = System.Drawing.ColorTranslator.FromHtml("#fbf991");
            //設備保養率 Color
            gv1.GetRow(gv1.RowCount - 3).BackColor = System.Drawing.ColorTranslator.FromHtml("#80e100");
            //設備當機率 Color
            gv1.GetRow(gv1.RowCount - 2).BackColor = System.Drawing.ColorTranslator.FromHtml("#ff7a00");
            //設備LOSS率 Color
            gv1.GetRow(gv1.RowCount - 1).BackColor = System.Drawing.ColorTranslator.FromHtml("#00ffff");

            #endregion

            #region 設定欄位寬度
            gv1.Width = Int32.Parse(gv1.Width) - 20 + "";
            Int32 width = 0;
            if (gv1.RowCount > 0)
            {
                TableRow tr = gv1.GetRow(0);
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    String Name = dt.Columns[i].ColumnName;
                    Int32 w = 0;
                    if (Name.Equals("Status"))
                        w = 80;
                    else
                        w = 120;
                    tr.Cells[i].Width = w;
                    width += w;
                }

                if (width < Int32.Parse(gv1.Width) - 80)
                    gv1.Width = (width + 80).ToString();
            }
            #endregion

            #region Event Code Pareto SbChart

            sbChart.Visible = true;

            DataTable dtChart = new DataTable();
            DataTable dtChart2 = new DataTable();


            dtChart = dt_calculate_time.Clone();
            dtChart2 = dt_calculate_time.Clone();

            foreach(DataRow dr in dt_calculate_time.Rows)
            {
                index = dt_calculate_time.Rows.IndexOf(dr);
                dtChart.ImportRow(dr);
                dtChart2.ImportRow(dr);
                dtChart.Rows[index][0] = dr[0] + "_sum";
                dtChart2.Rows[index][0] = dr[0] + "_sum";
            }

            sum = 0;
            foreach (DataColumn dc in dt_calculate_time.Columns)
            {
                index = dt_calculate_time.Columns.IndexOf(dc);
                dtChart.Columns[index].ReadOnly = false;
                dtChart2.Columns[index].ReadOnly = false;

                foreach (DataRow dr in dt_calculate_time.Rows)
                {
                    indexrow = dt_calculate_time.Rows.IndexOf(dr);
                    sum = 0;
                    if (index >= 2)
                    {
                        for (int i = 0; i <= dt_calculate_time.Rows.Count - 1; i++)
                        {
                            if (dt_calculate_time.Rows[i][index] != DBNull.Value)
                            {
                                sum += double.Parse(dt_calculate_time.Rows[i][index].ToString());
                            }
                        }
                        if (dt_calculate_time.Rows[indexrow][index] != DBNull.Value)
                        {
                            dtChart.Rows[indexrow][index] = Math.Round(double.Parse(dr[index].ToString()) * 100 / sum, 1, MidpointRounding.AwayFromZero);
                            dtChart2.Rows[indexrow][index] = double.Parse(dr[index].ToString()) * 100 / sum;
                        }
                        else
                        {
                            dtChart.Rows[indexrow][index] = 0;
                            dtChart2.Rows[indexrow][index] = 0;
                        }
                    }
                }
            }
            dtChart.Columns.Remove("C_DESC");
            dtChart.Columns.Remove("TAL");
            dtChart.Columns.Remove("PERCENT");
            dtChart2.Columns.Remove("C_DESC");
            dtChart2.Columns.Remove("TAL");
            dtChart2.Columns.Remove("PERCENT");

            sum = 0;
            foreach (DataColumn dc in dtChart2.Columns)
            {
                index = dtChart2.Columns.IndexOf(dc);
                foreach (DataRow dr in dtChart2.Rows)
                {
                    sum = 0;
                    indexrow = dtChart2.Rows.IndexOf(dr);
                    if (index == 0)
                    {
                        dr[0] = dr[0] + "_Line";
                    }
                    else
                    {
                        if (indexrow != 0)
                        {
                            if (dtChart2.Rows[indexrow][index] != DBNull.Value)
                            {
                                sum += double.Parse(dtChart2.Rows[indexrow][index].ToString()) + double.Parse(dtChart2.Rows[indexrow - 1][index].ToString());
                                dtChart2.Rows[indexrow][index] = Math.Round(sum, 1, MidpointRounding.AwayFromZero);
                            }
                        }
                    }
                }
            }

            DataTable dtStyle = dtChart.Copy();

            dtChart.Merge(dtChart2);

            DataTable dtStyle2 = dtChart2.Copy();
            for (int i = 1; i < dtChart.Columns.Count; i++)
                dtStyle.Columns.Remove(dtChart.Columns[i].ColumnName);

            for (int i = 1; i < dtChart2.Columns.Count; i++)
                dtStyle2.Columns.Remove(dtChart2.Columns[i].ColumnName);

            dtStyle.Columns[0].ColumnName = "SERIES_NAME";
            dtStyle.Columns.Add("SERIES_COLOR");
            dtStyle.Columns.Add("SERIES_TYPE");
            dtStyle.Columns.Add("AXISY");
            dtStyle.Columns.Add("TOOLTIP");

            dtStyle2.Columns[0].ColumnName = "SERIES_NAME";
            dtStyle2.Columns.Add("SERIES_COLOR");
            dtStyle2.Columns.Add("SERIES_TYPE");
            dtStyle2.Columns.Add("AXISY");
            dtStyle2.Columns.Add("TOOLTIP");

            String[] Colors = { "0,255,255", "255,208,255", "106,0,213" };

            foreach (DataRow dr in dtStyle.Rows)
            {
                index = dtStyle.Rows.IndexOf(dr);
                string[] RGB = Colors[index % Colors.Length].Split(',');
                dtStyle.Rows[index]["SERIES_COLOR"] = ColorTranslator.ToOle(Color.FromArgb(Int32.Parse(RGB[0]), Int32.Parse(RGB[1]), Int32.Parse(RGB[2])));
                dtStyle.Rows[index]["SERIES_TYPE"] = "BAR";
                dtStyle.Rows[index]["AXISY"] = "1";
                dtStyle.Rows[index]["TOOLTIP"] = "#SERIESNAME, #AXISLABEL = #VALY";
            }

            foreach (DataRow dr in dtStyle2.Rows)
            {
                index = dtStyle2.Rows.IndexOf(dr);
                string[] RGB = Colors[index % Colors.Length].Split(',');
                dtStyle2.Rows[index]["SERIES_COLOR"] = ColorTranslator.ToOle(Color.FromArgb(Int32.Parse(RGB[0]), Int32.Parse(RGB[1]), Int32.Parse(RGB[2])));
                dtStyle2.Rows[index]["SERIES_TYPE"] = "LINE";
                dtStyle2.Rows[index]["AXISY"] = "2";
                dtStyle2.Rows[index]["TOOLTIP"] = "#SERIESNAME, #AXISLABEL = #VALY";
            }

            dtStyle.Merge(dtStyle2);

            sbChart.AxisXTitle = "Accumulation Percentage(%)";
            sbChart.AxisYTitle = "Accumulation Percentage(%)";
            sbChart.DoubleAxisY = true;
            sbChart.ToolBar = true;
            sbChart.ToolBarTitle = m_ShopIndex + " Event Code 柏拉圖 Report Chart";
            sbChart.SetChart(dtChart, dtStyle);
            #endregion
        }
    }
}
