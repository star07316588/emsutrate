using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Drawing;
using MFGReportData;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;

public partial class FABReport_MOD_EmsUTRateDetail : System.Web.UI.Page
{
    string m_ShopIndex;
    protected void Page_Load(object sender, EventArgs e)
    {
        // *** Get Page Setting from System Info ***
        int index = 0;
        string strPageWidth = "0";
        string strReportType = "";
        DataTable dt_c;
        DataTable dtValue = null;

        if (Page.PreviousPage != null)
        {
            m_ShopIndex = txnQuery.GetPageInfo("Shop");
            strPageWidth = txnQuery.GetPageInfo("ReportWidth");
            ddmDetail.ImportFormInformation(txnQuery.ExportFormInformation());

            dt_c = txnQuery.GetQueryInfo();

            strReportType = Request.QueryString["ReportType"];

        }
        else
        {
            m_ShopIndex = ddmDetail.GetPageInfo("Shop");
            strPageWidth = ddmDetail.GetPageInfo("ReportWidth");
            dtValue = ddmDetail.GetSelectedValue();
            dt_c = ddmDetail.GetQueryInfo();
        }

        ddmDetail.ImportFormInformation(txnQuery.ExportFormInformation());

        RTData_Entity EntityObj = new RTData_Entity(m_ShopIndex);
        DataTable SelectedDT = ddmDetail.GetSelectedValue(); //上一頁被選取的欄位
        Int32 SelectedColumn = Int32.Parse(ddmDetail.GetSelectedColumn());
        Int32 SelectedRow = Int32.Parse(ddmDetail.GetSelectedRow());
        String SelectedEquipNBR = SelectedDT.Rows[0][0].ToString();
        String SelectedAlarmCode = SelectedDT.Rows[0][1].ToString();
        String SelectedValue = SelectedDT.Rows[0][Int32.Parse(ddmDetail.GetSelectedColumn())].ToString();
        DataTable dt = new DataTable();

        string strPeriod = ddmDetail.GetQueryInfo("qryPeriod", "FieldNameD");
        string strPeriod2 = ddmDetail.GetQueryInfo("qryPeriod", "TimeRangeSQLCondition");
        strPeriod += " " + strPeriod2;
        string strSQLCondition = "";
        List<string> lstSQLCondition = new List<string>();

        strSQLCondition = ddmDetail.GetQueryInfo("qShift", "SQLCondition");
        if (!String.IsNullOrEmpty(strSQLCondition)) lstSQLCondition.Add(strSQLCondition);
        strSQLCondition = ddmDetail.GetQueryInfo("qFloor", "SQLCondition");
        if (!String.IsNullOrEmpty(strSQLCondition)) lstSQLCondition.Add(strSQLCondition);
        strSQLCondition = ddmDetail.GetQueryInfo("qryAlarmFlag", "SQLCondition");
        if (!String.IsNullOrEmpty(strSQLCondition))
        {
            if (strSQLCondition.Contains("= N"))
            {
                lstSQLCondition.Add("ALM_ON_OFF_FLG = 'N'");
            }
            else if (strSQLCondition.Contains("= Y"))
            {
                lstSQLCondition.Add("ALM_ON_OFF_FLG = 'Y'");
            }
            else
            {
                lstSQLCondition.Add(strSQLCondition);
            }
        }
        strSQLCondition = ddmDetail.GetQueryInfo("qEquipment", "SQLCondition");
        if (!String.IsNullOrEmpty(strSQLCondition)) lstSQLCondition.Add(strSQLCondition);

        if (!String.IsNullOrEmpty(SelectedEquipNBR)) lstSQLCondition.Add("EQUIP_NBR = '" + SelectedEquipNBR + "'");
        if (!String.IsNullOrEmpty(SelectedAlarmCode)) lstSQLCondition.Add("ALM_CODE = '" + SelectedAlarmCode + "'");

        dt = EntityObj.GetEQAlarmHIS(strPeriod, lstSQLCondition);

        //沒有資料會有Error
        if (dt == null || dt.Rows.Count == 0)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "", "alert('No Data For Your Query')", true);
            return;
        }

        DataTable dtMapping = new RTData_Common().CreateMappingTable(dt);
        if (dtMapping != null && dt.Rows.Count > 0)
        {
            new RTData_Dictionary(m_ShopIndex).TranslateMappingTable(dtMapping);
            gv1.SetColumnMappingTable(dtMapping);
        }

        gv1.Title = m_ShopIndex + " Alarm Code Pareto Detail";
        gv1.ToolBarTitle = m_ShopIndex + " Alarm Code Pareto Detail";
        gv1.EnableScroll = true;

        gv1.SetData(dt);

        #region 設定欄位寬度

        gv1.Width = "40";
        gv1.Width = Int32.Parse(gv1.Width) - 20 + "";
        Int32 width = 0;
        if (gv1.RowCount > 0)
        {
            TableRow tr = gv1.GetRow(0);
            for (int i = 0; i < gv1.ColumnCount; i++)
            {
                Int32 w = 0;
                if (i == 0 || i == 1)
                    w = 120;
                else if (i == 2)
                    w = 200;
                else
                    w = 240;

                tr.Cells[i].Width = w;
                width += w;
            }
            gv1.Width = (width + 80).ToString();
        }
        #endregion
    }
}
