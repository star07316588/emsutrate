<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EmsUTRateQuery.aspx.cs" Inherits="FABReport_MOD_EmsUTRateQuery" %>
<%@ Register Src="~/UserControl/Query_Submit.ascx" TagName="QueryButton" TagPrefix="inx" %>
<%@ Register Src="~/UserControl/TransferManager.ascx" TagName="TransferManager" TagPrefix="inx" %> 
<%@ Register Src="~/UserControl/Query_RadioButtonList.ascx" TagName="RadioButtonList" TagPrefix="inx" %>
<%@ Register Src="~/UserControl/Query_ComboBox.ascx" TagName="ComboBox" TagPrefix="inx" %> 
<%@ Register Src="~/UserControl/Query_DateTime.ascx" TagName="DateTime" TagPrefix="inx" %> 
<%@ Register Src="~/UserControl/Query_Floor.ascx" TagName="Floor" TagPrefix="inx" %>
<%@ Register Src="~/UserControl/Query_Shift.ascx" TagName="Shift" TagPrefix="inx" %>
<%@ Register Src="~/UserControl/Query_Entity.ascx" TagName="Entity" TagPrefix="inx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>3.34 EMS Pareto Report</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <script src="../../Scripts/jquery.min.js" type="text/javascript"></script>
	<link href="../../Style/MFGReport.css" rel="stylesheet" type="text/css" />

        <script language="javascript" type="text/javascript">
        // To Set Query Area auto-fit Document Height
        $(document).ready(function () {
            ResizeQueryArea();
        });

        $(window).resize(function () {
            ResizeQueryArea();
        });

        function ResizeQueryArea() {
            var curHeight = $(window).innerHeight() - $("#divButton").outerHeight() - $("#divNotice").outerHeight() - 20;
            $("#divQueryArea").outerHeight(curHeight);
        }
        </script>
</head>
<body class="bgQuery">
    <form id="form1" runat="server">
       <asp:ScriptManager ID="qScriptManager" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
        <div id="divButton" align="right" style="width:100%; padding-top:5px;">
        <inx:QueryButton ID="btnQuery" runat="server" Text="Query" TransferTarget="txnQuery" />&nbsp;
        <inx:TransferManager ID="txnQuery" runat="server" TargetPage = "~/FABReport/MOD/EmsUTRateDataSet.aspx"  />
        <hr />
         </div>
        <div id="divQueryArea" style="width:100%; overflow:auto;">
          <table>
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <inx:DateTime ID="qryPeriod" runat="server"
                                Period="true" DateTimeType="D"
                                Title="Date" FieldNameD="acct_date" Width="250"/>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
           <tr>
		        <td>
                    <inx:Shift ID="qShift" runat="server" Width="250" Title="Shift" FieldName="SHIFT_ID" RepeatDirection="Horizontal"  />
		        </td>
            </tr>
            <tr>
                <td>
                    <inx:RadioButtonList ID="qryTimeType" runat="server" Title="Time Interval"  FieldName="TimeInterval" Width="250"/> 
                </td>
            </tr>
            <tr>
		        <td>
		            <inx:Floor ID="qFloor" runat="server" Width="250" Title="Floor" FieldName="FLOOR" />
		        </td>
            </tr>
              <tr>
                <td>
                   <inx:Entity ID="qEquipment" runat="server" Width="250" Title="Equipment" FieldName="EQUIP_NBR" IDListMode="MAIN8" />
                </td>                
            </tr>
          </table>
        </div>
    </form>
</body>
</html>
