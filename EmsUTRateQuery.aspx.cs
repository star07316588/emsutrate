using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MFGReportData;

public partial class FABReport_MOD_EmsUTRateQuery : System.Web.UI.Page
{
    string m_Shop;
    protected void Page_Load(object sender, EventArgs e)
    {
        m_Shop = Request.QueryString["Shop"];

        if (!IsPostBack)
        {
            qryPeriod.DefaultFrom = DateTime.Today.ToString("yyyy-MM-dd");
            qryPeriod.DefaultTo = DateTime.Today.ToString("yyyy-MM-dd");

            qEquipment.Shop = m_Shop;
            CreateTimeInterval();
        }
    }
    private void CreateTimeInterval() //qShowKind
    {
        string[] aryText = { "Shift", "Day", "Week", "Month", "Equipment", "Equipment+Shift" };
        string[] aryValue = { "Shift", "Day", "Week", "Month", "Equipment", "Equipment+Shift" };
        bool[] aryChecked = { true, false, false, false, false, false };
        SetItemList(qryTimeType, aryText, aryValue, aryChecked);
    }

    //--------------------- ���� --------------------------------------------------------------------
    private void SetItemList(object o, String[] aryText, String[] aryValue, bool[] aryChecked)
    {
        DataTable dt = new DataTable();
        DataColumn dcText = new DataColumn();
        dcText.ColumnName = "Text";
        dcText.DataType = typeof(string);
        dt.Columns.Add(dcText);
        DataColumn dcValue = new DataColumn();
        dcValue.ColumnName = "Value";
        dcValue.DataType = typeof(string);
        dt.Columns.Add(dcValue);
        DataColumn dcCheck = new DataColumn();
        dcCheck.ColumnName = "Checked";
        dcCheck.DataType = typeof(bool);
        dt.Columns.Add(dcCheck);

        for (int i = 0; i < aryText.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["Text"] = aryText[i];
            dr["Value"] = aryValue[i];
            dr["Checked"] = aryChecked[i];
            dt.Rows.Add(dr);
        }

        if (o.GetType().Name.Equals("usercontrol_query_radiobuttonlist_ascx"))
            ((ASP.usercontrol_query_radiobuttonlist_ascx)o).SetItemList(dt);
        else if (o.GetType().Name.Equals("usercontrol_query_combobox_ascx"))
            ((ASP.usercontrol_query_combobox_ascx)o).SetItemList(dt);
    }
}
