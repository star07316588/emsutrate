<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EmsUTRateDataSet.aspx.cs" Inherits="FABReport_MOD_EmsUTRateDataSet" %>

<%@ Register Src="~/UserControl/TransferManager.ascx" TagName="TransferManager" TagPrefix="inx" %>
<%@ Register Src="~/UserControl/DrillDownManager.ascx" TagName="DrillDownManager" TagPrefix="inx" %>
<%@ Register Src="~/UserControl/BasicGrid.ascx" TagName="BasicGrid" TagPrefix="inx" %>
<%@ Register Src="~/UserControl/StackBarChart.ascx" TagName="StackBarChart" TagPrefix="inx" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>4.16 Event Code Report DataSet </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link href="../../Style/MFGReport.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
            font-family: Tahoma,Verdana,Arial,sans-serif;
            font-size: 8px;
        }
    </style>
</head>
<body class="bgReport">
    <form id="form1" runat="server">
        <inx:TransferManager ID="txnQuery" runat="server" />
        <ul class="tabs">
            <li><a>StackBarChart</a></li>
            <li><a>DataTable</a></li>
        </ul>
        <div class="tab_container">
            <div class="tab_content">
                <inx:StackBarChart ID="sbChart" runat="server" ScrollBar="false" AxisYTitle="Yield Defect(%)" AxisY2Title="Accumulation Percentage" DoubleAxisY="true" AxisXTitle="EQUIP_NBR" EnableStack="true" Visible="false" LegendRight="true" />
            </div>

            <div class="tab_content">
                <inx:BasicGrid ID="gv1" runat="server" ToolBar="true" EnableScroll="true" Width="100%" Height="98%" DrillDownTarget="ddmDetail" DisableDrillDownCols="" Visible="false" />
            </div>
        </div>
        <asp:HiddenField ID="hReportWidth" runat="server" />
        <asp:HiddenField ID="hReportHeight" runat="server" />
    </form>
    <inx:DrillDownManager ID="ddmDetail" runat="server" PostBackTarget="_blank" PostBackUrl="EmsUTRateDetail.aspx" />
</body>
</html>
<script type="text/javascript">
    var showTab = 0; // �w�] Tab
    var defaultLi = $('ul.tabs li').eq(showTab).addClass('active');
    $('.tab_content').eq(defaultLi.index()).siblings().hide();

    $('ul.tabs li').mouseover(function () {
        var index = $(this).index();
        $(this).addClass('active').siblings('.active').removeClass('active');
        $('.tab_content').eq(index).stop(false, true).fadeIn().siblings().hide();
        return false;
    }).find('a').focus(function () {
        this.blur();
    });
</script>
