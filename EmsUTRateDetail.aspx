<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EmsUTRateDetail.aspx.cs" Inherits="FABReport_MOD_EmsUTRateDetail" %>
<%@ Register Src="~/UserControl/BasicGrid.ascx" TagName="BasicGrid" TagPrefix="inx" %>  
<%@ Register Src="~/UserControl/DrillDownManager.ascx" TagName="DrillDownManager" TagPrefix="inx" %>
<%@ Register Src="~/UserControl/TransferManager.ascx" TagName="TransferManager" TagPrefix="inx" %> 

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>INX MFG Report - Alarm Code Pareto</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" charset="utf-8"/>
    <link href="../../Style/MFGReport.css" rel="stylesheet" type="text/css" />
    <style>   
    body 
    {
        margin:0;padding:0;font-family: Tahoma,Verdana,Arial,sans-serif;font-size: 8px;
    }
    </style>
</head>
<body style="margin:0;padding:0;overflow:hidden;">
    <form id="form1" runat="server">
    <div>
    <inx:BasicGrid ID="gv1" runat="server"  ToolBar="true" EnableScroll="true" Width="100%"  />

    </div>
    <inx:TransferManager ID="txnQuery" runat="server" />
    </form>

    <inx:DrillDownManager ID="ddmDetail" runat="server" />
</body>
</html>
